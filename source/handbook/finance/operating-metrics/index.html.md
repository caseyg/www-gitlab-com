---
layout: markdown_page
title: "Operating Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Metrics Definitions and Review Process
We track a wide range of metrics on our corporate dashboard. Many definitions are self evident but some are not.

### Annual Recurring Revenue (ARR)
Recurring revenue recognized in current month multiplied by 12.

### Days Sales Outstanding (DSO)
Average Accounts Receivable balance over prior 3 months divided by Total Contract Value (TCV) bookings over the same period mutilpied by 90 that provides an average number of days that customers pay their invoices.  Link to a good [definition](https://www.investopedia.com/terms/d/dso.asp)  and [Industry guidance](https://www.opexengine.com/software-industry-revenue-growth-accelerating-and-hiring-expected-to-jump-according-to-new-siiaopexengine-report/) suggests the median DSO for SAAS companies is 76 days. Our target at GitLab is 45 days.

### Average Sales Price (ASP)
IACV per won deal.

### Total Contract Value (TCV)
All bookings in period (including multiyear); bookings is equal to billings with standard payment terms.

### Annual Contract Value (ACV)
Current Period subscription bookings which will result in revenue over next 12 months. For multiple year deals with contracted ramps, the ACV will be the average annual booking per year.

### Incremental Annual Contract Value (IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months less any non-renewals, cancellations or any other decrease to annual recurring revenue. Excluded from IACV are bookings that are non-recurring such as professional services, training and non-recurring engineering fees (NRE). Also equals ACV less renewals. However, bookings related to true up licenses, although non-recurring, are included in IACV.

### Retention, Net (Dollar Weighted)
Current period MRR plus true-ups from customers that were on board 12 months prior divided by MRR plus true-ups from 12 months prior.

### Retention, Gross (Dollar Weighted)
Remaining cohorts from Actual Subscription customers active as of 12 months ago multiplied by revenue from 12 months ago divided by actual Subscription customers as of date 12 months prior multiplied by revenue from 12 months ago. [Industry guidance]("http://www.forentrepreneurs.com/saas-metrics-2/") suggests median gross dollar churn performance for SaaS/subscription companies is 8% per year

### Customer Acquisition Cost (CAC)
Total Sales & Marketing Expense/Number of New Customers Acquired

### CAC Ratio
Total Sales & Marketing Expense/ACV from new customers (excludes growth from existing).  [Industry guidance](http://www.forentrepreneurs.com/2017-saas-survey-part-1/) reports that median performance is 1.15 with anything less than 1.0 being considered very good.

### LTV to CAC Ratio
The customer Life-Time Value to Customer Acquisition Cost ratio (LTV:CAC) measures the relationship between the lifetime value of a customer and the cost of acquiring that customer. [A good LTV to CAC ratio is considered to be > 3.0.](https://www.klipfolio.com/resources/kpi-examples/saas-metrics/customer-lifetime-value-to-customer-acquisition-ratio)

### Rep Productivity
Monthly IACV / number of quota carrying sales representatives on board for at least 90 days prior to start of period.

### Magic Number
IACV for trailing three months / Sales & Marketing Spend over trailing months -6 to months -4 (one quarter lag). [Industry guidance](http://www.thesaascfo.com/calculate-saas-magic-number/) suggests a good Magic Number is > 1.0.

### MQL
[Marketo Qualified Lead](https://about.gitlab.com/handbook/business-ops/#customer-lifecycle)

### SQL
[Sales Qualified Lead](https://about.gitlab.com/handbook/business-ops/#customer-lifecycle)

### Cost per MQL
Marketing expense divided by the number of MQLs

### Sales efficiency ratio
IACV / sales and marketing spend. [Industry guidance](http://tomtunguz.com/magic-numbers/) suggests that average performance is 0.8 with anything greater than 1.0 being considered very good.

### Marketing efficiency ratio
IACV / marketing spend

### Field efficiency ratio
IACV / sales spend

### LTV
Customer Life-Time Value = Average Revenue per Year x Gross Margin% x 1/(1-K) + GxK/(1-K)^2; K = (1-Net Churn) x (1-Discount Rate).  GitLab assumes a 10% cost of capital based on current cash usage and borrowing costs.

### Capital Consumption
TCV less Total Operating Expenses.  This metric tracks net cash consumed excluding changes in working capital (i.e. burn due to balance sheet growth). Since the growth in receivables can be financed with using cheap debt instead of equity is a better measure of capital efficiency than cash burn.

### Monthly Metrics Review

Purpose:  We review all key operating metrics that either i) appear in the Corporate metrics sheet or ii) are included in the our Operating Model. The goal for the monthly meeting is to understand month to month variances as well as variances against the plan, forecast and operating model.

Agenda:
1. Review metric and conclude on implications for operating model.
1. Discuss proposals for different measurement.
1. Determine if external benchmarks are required.
1. Discuss proposals for addition of new metrics.
1. Discuss proposals for deprecation of existing metrics

Timing:  Meetings are monthly starting on the 10th day after month end.

Invitees:  Mandatory attendees are the owner of the metric and eteam functional representative and the CFO.  Optional attendees are the rest of the e-team and anyone who has an interest in the metric.

Process:  The owner of the metric(s) drives the meeting and is responsible for updating the doc with necessary data, links, MRs, etc. Calendar invites should be editable by invitees.  Use the CFO Zoom number.
