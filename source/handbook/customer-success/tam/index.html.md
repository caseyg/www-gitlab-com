---
layout: markdown_page
title: "Technical Account Management"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, implementation engineers and support. 

See the [Technical Account Manager role description](/roles/sales/technical-account-manager/) for further information.

### Advocacy
A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product.  In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

### Values

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption, training and regular healthchecks.

### Responsibilities and Services
All Premium customers with a minimum ARR of $100,000 are aligned with a Technical Account Manager. There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

#### Relationship Management
* Regular cadence calls
* Regular open issue reviews and issue escalations
* Account healthchecks
* Quarterly business reviews
* Success strategy roadmaps - beginning with a 30/60/90 day success plan
* To act as a key point of contact for guidance, advice and as a liason between the customer and other GitLab teams
* Own, manage, and deliver the customer onboarding experience
* Help GitLab's customers realize the value of their investment in GitLab
* GitLab Days

#### Training
* Identification of pain points and training required
* Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
* "Brown Bag" trainings
* Regular communucation and updates on GitLab features
* Product and feature guidance - new feature presentations

#### Support
* Upgrade planning
* User adoption strategy
* Migration strategy and planning
* Launch support
* Monitors support tickets and ensures that the customer receives the appropriate support levels
* Support ticket escalations

It is also possible for a customer to pay for a Technical Account Manager's services in order to receive priority, "white glove" assistance, guidance and support as well as more time allocated to their account on a monthly basis. There are also additional services a Technical Account Manager will provide to the services listed above. These are currently to be determined and will become available before the end of the second quarter of 2018.

---

# Engagement Models
There are three models currently offered for Technical Account Manager engagement. These are broken into tiers that currently use Annual Recurring Revenue as a metric for determining a manageable volume for a single Technical Account Manager and the depth of involvement during the engagement.

## Table of Technical Account Manager Engagements
Technical Account Managers focus on [large and strategic accounts](/handbook/sales/#market-segmentation), [small business (SMB) and mid-market accounts](/handbook/sales/#market-segmentation) are typically handled by [Account Managers](/handbook/customer-success/account-management). 

||TIER 1 |TIER 2 |TIER 3 |
|:---|:---|:---|:---|
|Minimum ARR|$500,000|$250,000|$100,000|

## Managing the Customer Engagement
Technical Account Managers will typically manage customer engagements via a GitLab project in the [`account-management` group](https://gitlab.com/gitlab-com/account-management/). This project will be based off a [customer collaboration project template](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) and then customized to match the customer's needs as outlined above. The project is pre-loaded with milestones, issues, labels and a README template to help kick off the project and outline a proof of concept, implementation and customer onboarding.

### To start a new customer engagement:
1.  Somewhere between step 3 and step 7 of the customer journey sequence, a Solutions Architect should create a project for the customer in GitLab and include an Implementation Engineer and Technical Account Manager who're best aligned with the customer account.
2. Go to the [customer collaboration project template project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template).
3. Follow the steps in the initial README.md file to create the appropriate project type (post sales or pre sales).

### Where does a Technical Account Manager fit in?
During the pre-sales process, a Solutions Architect owns the project with assistance from the Strategic Account Leader and should include the Implementation Engineer if there is one assigned. During the pre-sales project, a Technical Account Manager is involved but only for visibility. Until the account becomes a paying customer, the project remains in pre-sales. Once the customer has paid, the Strategic Account Leader will set up the Welcome to GitLab call along with the key GitLab employees (SAL, SA, IE and Technical Account Manager) and the customer. There is a preloaded issue for this in the project template.

This call will introduce the customer to the Technical Account Manager and begin the handover process. The Technical Account Manager will then lead the rest of the call and own the project going forward. The project is then moved from a pre-sales project under the [`pre-sales account-management` group](https://gitlab.com/gitlab-com/account-management/pre-sales) to a post-sales project under [`account-management` group](https://gitlab.com/gitlab-com/account-management).

Typically this project template isn't used for small business or mid-market accounts. However, if an Account Manager feels this could be useful to assist their relationship with the customer, then they can utilise it and edit down as needed. 

### Salesforce - Customer Success Section

On an account view in Salesforce, there is a Customer Success section, with the following fields:

* Health Score - A field to record the overall customer health (details TBD on this)
* GitLab Customer Success Project - Where you enter the URL of the project created using the template described above
* Customer Slack Channel - A field to record Slack channel(s) used for internal and external customer collaboration. If a channel is internal, make sure it follows the naming convention"#organisation-name-internal"
* Solutions Architect - The Solutions Architect aligned with the acccount
* Technical Account Manager	- The Technical Account Manager aligned with the acccount

## The Customer Meta-Record
The concept of a customer meta-record is how a Technical Account Manager develops and maintains a holistic understanding of customer accounts.  It is a living record that includes both business and technical information about the customer.  The data captured here informs not only the Technical Account Manager, but all of GitLab about critical details to the success of our customers.

## Business Profile
The business profile is captured during the discovery and scoping stages in the pre-sales phase of the customner lifecycle.  Once an opportunity is set to closed-won, the Technical Account Manager is largely responsible for maintaining this data. The business profile of a customer's meta-record contains data points related to their organizational structure, the internal advocate/champion, the features most compelling to the customer, the "why" of purchasing GitLab, objectives for the implementation and critically, the data captured in the customer feedback loop.

## Technical Profile
The technical profile includes objective data points about a customers technical landscape and can be captured at any stage of the customer lifecycle. Specifically, items related to architecture, sizing, scale, security and compliance requirements are captured in the technical profile. In general, solution architects and implementation specialists are primarily responsible for collecting and capturing information for the technical profile. This information is transitioned to the Technical Account Manager once a customer goes live and the Technical Account Manager maintains the information throughout the remainder of the customer lifecycle.

## Automated Customer Discovery Tools
A series of data sheets and automation scripts is being developed that will streamline much of the intake, discovery and reconnaissance activities during the pre-sales stages of the customer lifecycle.  Decisions surrounding where this data will reside and which information is part of the critical path during customer onboarding are currently underway.
